import java.lang.Math;
import java.util.*;

public class CardGenerator{
  public static void main(String[] args) {
      int cardNumber = (int) ((Math.random()*13)+2);
      System.out.println(cardNumber);
      int cardSuit = (int)(Math.random() * 4) + 1;
      System.out.println(cardSuit);
      String suit;
      String num;
      if (cardSuit == 1){
        suit = "Hearts";}
      else if (cardSuit == 2){
        suit = "Diamonds";}
      else if (cardSuit == 3){
        suit = "Spades";}
      else {suit = "Clovers";}
      switch(cardNumber){
        case 14: 
          num = "Ace";
          break;
        case 13: 
          num = "King";
          break;
          
        case 12: num = "Queen";
            break;
        case 11: num = "Jack";
            break;
        default: num = ""+cardNumber;
      }
      System.out.println("You picked the " + num + " of " + suit);
  }
}